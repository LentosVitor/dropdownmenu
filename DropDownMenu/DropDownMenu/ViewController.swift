//
//  ViewController.swift
//  DropDownMenu
//
//  Created by Vitor Lentos on 16/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var selectFruitBtn: UIButton!
    @IBOutlet var fruitBtnCollection: [UIButton]!
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        selectFruitBtn.layer.cornerRadius = selectFruitBtn.frame.height / 2;
        fruitBtnCollection.forEach{ (btn) in
            btn.layer.cornerRadius = btn.frame.height / 2;
            btn.isHidden = true;
            btn.alpha = 0;
        }
        
    }
    
    //MARK:- Actions

    @IBAction func selectFruitPressed(_ sender: UIButton) {
        fruitBtnCollection.forEach { (btn) in
            UIView.animate(withDuration: 0.7){
                btn.isHidden = !btn.isHidden;
                btn.alpha = btn.alpha == 0 ? 1 : 0;
                btn.layoutIfNeeded();
            }
        }
    }
    
    
    @IBAction func fruitPressed(_ sender: UIButton) {
        if let btnLbl = sender.titleLabel?.text {
            print(btnLbl);
            selectFruitBtn.setTitle(btnLbl, for: .normal);
            fruitBtnCollection.forEach { (btn) in
                UIView.animate(withDuration: 0.7){
                    btn.isHidden = !btn.isHidden;
                    btn.alpha = btn.alpha == 0 ? 1 : 0;
                    btn.layoutIfNeeded();
                }
            }
        }
    }
    
}

